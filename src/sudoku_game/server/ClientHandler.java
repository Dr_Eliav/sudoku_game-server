
package sudoku_game.server;

import Enums.Result;
import java.net.*;
import java.io.*;
import ObjectsSerialization.*;
import java.util.LinkedList;

/**
 * 
 * @author EliavBuskila
 */

class ClientHandler implements Runnable {

    // Connection information
    private final Socket s;
    private final ObjectInputStream ois;
    private final ObjectOutputStream oos;
    
    // Request information
    private Information info;
    private final ServerLogic sl;
    private final SqlStoredProcedure sqlf;
    private Result result;
    
    //contractor get connection information from server
    public ClientHandler(Socket s, ObjectInputStream ois, ObjectOutputStream oos) {
        this.s = s;
        this.ois = ois;
        this.oos = oos;
        this.sl = new ServerLogic();
        this.sqlf = new SqlStoredProcedure();
    }

    @Override
    public void run() {
        try {
            // get request
            info = (Information) ois.readObject();
            System.err.println("client send: \n" + info.toString());
            
            // Selects an appropriate function that will go in SQL
            switch (info.getRequest()) {
                case ADD_USER:
                    result = sl.checkDetailsRegister((User)info.getInfo());
                    if (result != Result.SUCCESS) break;
                    result = sqlf.addUser((User)info.getInfo());
                    break;
                case SEND_CODE:
                    result = sl.sendEmail((User)info.getInfo());
                    break;
                case USER_LOGIN:
                    result = sl.checkDetailsLogin((User)info.getInfo());
                    if (result != Result.SUCCESS) break;
                    result = sqlf.userLogin((User)info.getInfo());
                    break;
                case GUEST_LOGIN:
                    result = sqlf.guestLogin((Guest)info.getInfo());
                    break;
                case GET_STATISTIC:
                    result = sqlf.getStatistic((Player)info.getInfo(), (Statistic)info.getInfo(1));
                    break;
                case GET_ALL_BOARDS:
                    result = sqlf.getAllBoards((Player)info.getInfo(), (LinkedList<Board>)info.getInfo(1));
                    break;
                case RANDOM_BOARD:
                    result = sl.sudokuMaker((Board)info.getInfo(), (int)info.getInfo(1));
                    break;
                case ADD_BOARD:
                    result = sqlf.addBoard((Board)info.getInfo());
                    break;
                case GET_GAME:
                    result = sqlf.getGame((Player)info.getInfo(), (Board)info.getInfo(1), (Game)info.getInfo(2));
                    break;
                case CHECK_SOLUTION:
                    result = sl.checkSolution((Game)info.getInfo());
                    break;
                case SOLVE_BOARD:
                    result = sl.sudokuSolver((Board)info.getInfo());
                    break;
                case UPDATE_GAME:
                    result = sqlf.updateGame((Game)info.getInfo());
                    break;
                case UPDATE_USERNAME:
                    if (sl.cheackUsername(((Player)info.getInfo()).getUsername())) {
                        result = Result.ERROR;
                    } else {
                        result = sqlf.updateUsername((Player)info.getInfo());
                    }
                    break;
                case UPDATE_PASSWORD:
                    if (sl.checkPassword(((User)info.getInfo()).getPassword())) {
                        result = Result.ERROR;
                    } else {
                        result = sqlf.updatePassword((User)info.getInfo());
                    }
                    break;
                case LOGOUT:
                    result = sqlf.loguot((Player)info.getInfo());
                    break;
                default:
                    System.err.println("Request not found");
                    break;
            }
            // Returns result 
            info.setResult(result);
            System.err.println("server send: \n" + info.toString());
            oos.writeObject(info);
            oos.flush();
            
            // close connection
            s.close();
            ois.close();
            oos.close();
        } catch (IOException |ClassNotFoundException ex) {
            System.err.println("ClientHandler - Error" + ex);
        }
    }
}
