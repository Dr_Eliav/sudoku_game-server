
package sudoku_game.server;

import Enums.Result;
import ObjectsSerialization.*;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

/**
 * Responsible for the logical actions that the server performs,
 * creating a sudoku board, sending an email, checking a solution,
 * data validation, and so on
 * The requests arrive and are sent back to ClientHandler.java
 * 
 * @author EliavBuskila
 */
public class ServerLogic {
    
    public Result checkDetailsLogin(User login) {
        if (login.getUsername().length() < 5 || login.getUsername().length() > 25)
            return Result.ERROR_USER_LOGIN;
        if (login.getPassword().length() < 8 || login.getPassword().length() > 25)
            return Result.ERROR_USER_LOGIN;
        return Result.SUCCESS;
    }
    public boolean cheackUsername(String username) {
        return (username.length() < 5 || username.length() > 25);
    }
    public boolean checkPassword(String password) {
        return (password.length() < 8 || password.length() > 25);
    }
    public Result checkDetailsRegister(User register) {
        try {
            if (cheackUsername(register.getUsername()))
                return Result.ERROR_REGISTER;
            if (checkPassword(register.getPassword()))
                return Result.ERROR_REGISTER;
            InternetAddress emailAddr = new InternetAddress(register.getEmail());
            emailAddr.validate();
            return Result.SUCCESS;
        } catch (AddressException ex) {
            return Result.ERROR_REGISTER;
        }
    }
    public Result sendEmail(User user) {
        
        final String username = "Sudoku.Game.111@gmail.com";
        final String password = "SudokuGame111";
        
        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session;
        session = Session.getInstance(props,
        new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(username, password);
            }
        });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(user.getEmail()));
            message.setSubject("Code To Sudoku Game");
            message.setText("Dear User," + "\n\n your code is: " + user.createCode());
            Transport.send(message);
            System.out.println("ServerLogic.sendEmail - " + Result.SUCCESS);
            return Result.SUCCESS;
        } catch (MessagingException e) {
            System.out.println(e);
            System.out.println("ServerLogic.sendEmail - " + Result.ERROR);
            return Result.ERROR_SEND_EMAIL;
        }
    }
    public Result sudokuMaker(Board board, int level) {
        Sudoku sm = new Sudoku(Board.SIZE,(level+3)*10);
        sm.complete();
        if (sm.isSolved()) {
            board.setStrart(sm.getStart());
            board.setSolution(sm.getSolution());
            return Result.SUCCESS;
        } else {
            return Result.ERROR_MAKER;
        }
    }
    public Result checkSolution(Game game) {
        if (checkSudokuStatus(game.getSaveBoard())) {
            game.setIsSolved(true);
            return Result.SUCCESS;
        } else {
            game.setIsSolved(false);
            game.addNumberOfMistakes(1);
            return Result.ERROR_SOLUTION;
        }
    }
    public Result sudokuSolver(Board board) {
        Sudoku ss = new Sudoku(board.getStrart(), Board.SIZE);
        if (ss.isSolved()) {
            board.setSolution(ss.getSolution());
            return Result.SUCCESS;
        }
        return Result.ERROR_SOLVER;
    }
    private boolean checkSudokuStatus(int[][] board) {
        int sqrtSize = (int)Math.sqrt(Board.SIZE);
        for (int i = 0; i < Board.SIZE; i++) {
            int[] row = new int[9];
            int[] square = new int[9];
            int[] column = board[i].clone();

            for (int j = 0; j < Board.SIZE; j++) {
                row[j] = board[j][i];
                square[j] = board[(i/sqrtSize)*sqrtSize+(j/sqrtSize)][(i*sqrtSize)%Board.SIZE+(j%sqrtSize)];
            }
            if (!(validate(column) && validate(row) && validate(square)))
                return false;
        }
        return true;
    }
    private boolean validate(int[] check) { 
        Arrays.sort(check);
        for(int i = 0; i < Board.SIZE; i++){
            if(check[i] != (i+1)) {
                return false;
            }
        }
        return true;
    }
}
