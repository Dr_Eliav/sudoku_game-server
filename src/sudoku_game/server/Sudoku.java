
package sudoku_game.server;

import ObjectsSerialization.Board;
import java.util.Random;

/**
 * Responsible for creating a Sudoku board,
 * and solving a Sudoku board.
 * 
 * @author EliavBuskila
 */
public class Sudoku {
    
    private Random rand;
    
    private final int[] start[];
    private final int[] solution[];
    private final int size;
    private final int sqrtSize;
    private int remove;
    private boolean isSolved;

    // for maker
    public Sudoku(int size, int remove) {
        this.rand = new Random();
        this.start = new int[size][size];
        this.solution = new int[size][size];
        this.size = size;
        this.sqrtSize = (int) Math.sqrt(size);
        this.remove = remove;
    }
    // for solver
    public Sudoku(int[][] board, int size) {
        this.start = new int[size][size];
        Board.copyBoard(start, board);
        this.solution = new int[size][size];
        Board.copyBoard(solution, board);
        this.size = size;
        this.sqrtSize = (int) Math.sqrt(size);
        if (isLegalBoard())
            this.isSolved = solve(0, 0);
        else this.isSolved = false;
    }
    
    // for make sudoku
    private int getRandom(int until) {
        until = rand.nextInt(until+1);
        return (until == size*size) ? (until-1) : (until);
    }  
    private void completeSquare(int row, int col) {
        int digit;
        for (int i = 0; i < sqrtSize; i++) {
            for (int j = 0; j < sqrtSize; j++) {
                do {
                    digit = getRandom(size);
                } while (!isUsedInSquare(row, col, digit));
                solution[row+i][col+j] = digit;
            }
        }
    }
    private void completeDiagonals() {
        for (int i = 0; i < size; i += sqrtSize) {
            completeSquare(i, i);
        }
    }
    boolean CompleteAnotherSquare(int row, int col) { 
            // System.out.println(row+" "+col); 
            // ירידת שורה באינדקסים של הלוח
            if (col>=size && row<size-1) { 
		row++; 
		col = 0; 
            } 
            // כשאר אין יותר שורות וכל הלוח נסרק
            if (row>=size && col>=size) 
		return true; 
            
            // ירידת שורה בתוך קוביות שתיים ושלוש
            if (row < sqrtSize) { 
		if (col < sqrtSize) 
                    col = sqrtSize; 
            // קפיצה על קובייה חמש
            } else if (row < size-sqrtSize) { 
		if (col==(int)(row/sqrtSize)*sqrtSize) 
                    col = col + sqrtSize; 
            // ירידת שורה בתוך קוביות שבע ושמונה
            } else { 
		if (col == size-sqrtSize) { 
                    row++; 
                    col = 0; 
                    // אם אין עטד שורות הלוח נסרק בהצלחה
                    if (row>=size) return true; 
                } 
            } 
            // נסיון להכניס סיפרה לאינדקס
            for (int num = 1; num<=size; num++) { 
                // אם זה אפשרי
                if (isLegalMove(row, col, num)) { 
                    solution[row][col] = num;
                    // האינדקס הבא
                    if (CompleteAnotherSquare(row, col+1)) 
			return true; 
                    solution[row][col] = 0; 
		} 
            }
            // אם לא ניתן להכניס לאינדקסים מסויימים סיפרה יוחזר שקר
            return false; 
	} 
    private void removeDigits(){
        int count = remove;
        while (count > 0) {            
            int index = getRandom(size*size);
            int i = (index/size);
            int j = (index%size);
            
            if(start[i][j] != 0) {
                start[i][j] = 0;
                count--;
            }
        }
    }
    public void complete() {
        completeDiagonals();
        if (!CompleteAnotherSquare(0, sqrtSize)) {
            isSolved = false;
        } else {
            isSolved = true;
            Board.copyBoard(start, solution);
            removeDigits();
        }
    }
    
    private boolean isUsedInRow(int i, int digit){
        for (int j = 0; j < size; j++) {
            if(solution[i][j] == digit) {
                return false;
            }
        }
        return true;
    }
    private boolean isUsedInCol(int j, int digit){
        for (int i = 0; i < size; i++) {
            if(solution[i][j] == digit) {
                return false;
            }
        }
        return true;
    }
    private boolean isUsedInSquare(int rowStart, int colStart, int digit) { 
            for (int i = 0; i<sqrtSize; i++) {
		for (int j = 0; j<sqrtSize; j++) { 
                    if (solution[rowStart+i][colStart+j]==digit) {
			return false; 
                    }
                }
            }
            return true; 
    } 
    private boolean isLegalMove(int i, int j, int digit) {
        return (isUsedInRow(i, digit) 
                && isUsedInCol(j, digit) 
                && isUsedInSquare(i-(i%sqrtSize), j-(j%sqrtSize), digit));
    }
    
    private boolean isLegalBoard() {
        int digit;
        for (int row = 0; row < size; row++) 
            for (int col = 0; col < size; col++) { 
                if (solution[row][col] != 0) {
                    digit = solution[row][col];
                    solution[row][col] = 0;
                    if (!isLegalMove(row, col, digit)) {
                        solution[row][col] = digit;
                        return false;
                    }
                    solution[row][col] = digit;
                }
            }
        return true;
    }
    private boolean isEmptyLocation(int i, int j) {
        return (start[i][j] == 0);
    }
    private boolean solve(int i, int j) {
        
        if (j == size) {
            j = 0; i++;
            if (i == size) return true;
        }
        while (!isEmptyLocation(i, j)) {
            return solve(i, j+1);
        }
        for (int digit = 1; digit < (size+1); digit++)
            if (isLegalMove(i, j, digit)) {
                solution[i][j] = digit;
                
                if (!solve(i, j+1)) solution[i][j] = 0;
                else return true;
            }
        return false;
    }
    
    public int[][] getStart() {
        return start;
    }
    public int[][] getSolution() {
        return solution;
    }
    public boolean isSolved() {
        return isSolved;
    }
}
