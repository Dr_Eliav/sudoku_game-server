
package sudoku_game.server;

import Enums.Request;
import Enums.Result;
import ObjectsSerialization.*;
import java.sql.*;
import java.util.LinkedList;

/**
 * This class is responsible for opening and closing a connection with the SQL
 * Sending information and receiving information from SQL
 * The requests arrive and are sent back to ClientHandler.java
 * 
 * @author EliavBuskila
 */
public class SqlStoredProcedure {
        
    private static final String DATABASE_NAME = "Sudoku_Game";
    private static final String STRING_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    private static final String STRING_CNNECTION = "jdbc:sqlserver://localhost:1433;databaseName="+DATABASE_NAME+";IntegratedSecurity=true";

    // open & close connection sql
    private Connection open(){
        Connection connection = null;
        try {
            Class.forName(STRING_DRIVER);
            connection = DriverManager.getConnection(STRING_CNNECTION);
            System.err.println("SqlStoredProcedure.open - SUCCESS");
        } catch (ClassNotFoundException | SQLException ex) {
            System.err.println("SqlStoredProcedure.open - failed \n" + ex);
        }
        return connection;
    }
    private void close(Connection connection, PreparedStatement preparedStatement, ResultSet resultSet) {   
        try {          
            if (resultSet != null) resultSet.close();
            preparedStatement.close();
            connection.close();
            System.err.println("SqlStoredProcedure.close - SUCCESS");
        } catch (SQLException ex) {
            System.err.println("SqlStoredProcedure.close \n - failed" + ex);
        }
    }
    public Result addUser(User user) {
        Result res = Result.ERROR;
        try {
            // open connection to database
            Connection connection = this.open();
            // send data to database
            String procedureName = Request.ADD_USER.SP();
            PreparedStatement preparedStatement = connection.prepareStatement(procedureName);
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getEmail());
            
            ResultSet resultSet = preparedStatement.executeQuery();
            // get result from database
            if (resultSet.next()) {
                user.setID(resultSet.getInt(1));
                res = Result.getResult(resultSet.getInt(1));
            }
            // close connection
            this.close(connection, preparedStatement, resultSet);
            System.err.println("SqlStoredProcedure.addUser - " + res);
        } catch (SQLException ex) {
            res = Result.ERROR;
            System.err.println("SqlStoredProcedure.addUser - " + ex);
        }
        return res;
    }
    public Result userLogin(User user) {
        Result res = Result.ERROR;
        try {
            // open connection to database
            Connection connection = this.open();
            // send data to database
            String procedureName = Request.USER_LOGIN.SP();
            PreparedStatement preparedStatement = connection.prepareStatement(procedureName);
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getPassword());
            
            ResultSet resultSet = preparedStatement.executeQuery();
            // get result from database
            if (resultSet.next()) {
                user.setID(resultSet.getInt(1));
                res = Result.getResult(resultSet.getInt(1));
            }
            // close connection
            this.close(connection, preparedStatement, resultSet);
            System.err.println("SqlStoredProcedure.userLogin - " + res);
        } catch (SQLException ex) {
            res = Result.ERROR;
            System.err.println("SqlStoredProcedure.userLogin - " + ex);
        }
        return res;
    }
    public Result guestLogin(Guest guest) {
        Result res = Result.ERROR;
        try {
            // open connection to database
            Connection connection = this.open();
            // send data to database
            String procedureName = Request.GUEST_LOGIN.SP();
            PreparedStatement preparedStatement = connection.prepareStatement(procedureName);
            preparedStatement.setString(1, guest.getMAC());
            
            ResultSet resultSet = preparedStatement.executeQuery();
            // get result from database
            if (resultSet.next()) {
                guest.setID(resultSet.getInt(1));
                guest.setUsername(resultSet.getString(2));
                res = Result.getResult(resultSet.getInt(1));
            }
            // close connection
            this.close(connection, preparedStatement, resultSet);
            System.err.println("SqlStoredProcedure.guestLogin - " + res);
        } catch (SQLException ex) {
            res = Result.ERROR;
            System.err.println("SqlStoredProcedure.guestLogin - " + ex);
        }
        return res;
    }
    public Result getStatistic(Player player, Statistic statistic) {
        Result res = Result.ERROR;
        try {
            // open connection to database
            Connection connection = this.open();
            // send data to database
            String procedureName = Request.GET_STATISTIC.SP();
            PreparedStatement preparedStatement = connection.prepareStatement(procedureName);
            preparedStatement.setInt(1, player.getID());
        
            ResultSet resultSet = preparedStatement.executeQuery();
            // get result from database
            if (resultSet.next()) {
                statistic.setUsername(resultSet.getString(1));
                statistic.setTotalPlayingTime(resultSet.getString(2));
                statistic.setTotalSolvedGames(resultSet.getString(3));
                statistic.setCompletion(resultSet.getString(4));
                res = Result.SUCCESS;
            }
            // close connection
            this.close(connection, preparedStatement, resultSet);
            System.err.println("SqlStoredProcedure.getStatistic - " + res);
        } catch (SQLException ex) {
            res = Result.ERROR;
            System.err.println("SqlStoredProcedure.getStatistic - " + ex);
        }
        return res;
    }
    public Result addBoard(Board board) {
        Result res = Result.ERROR;
        try {
            // open connection to database
            Connection connection = this.open();
            // send data to database
            String procedureName = Request.ADD_BOARD.SP();
            PreparedStatement preparedStatement = connection.prepareStatement(procedureName);
            preparedStatement.setString(1, board.getStringStart());
            preparedStatement.setString(2, board.getStringSolution());
            
            ResultSet resultSet = preparedStatement.executeQuery();
            // get result from database
            if (resultSet.next()) {
                board.setBoardID(resultSet.getInt(1));
                board.setNumberOfSolvers(0);
                board.setStatus("unsolved");
                board.setBestTime("---");
                res = Result.getResult(resultSet.getInt(1));
            }
            // close connection
            this.close(connection, preparedStatement, resultSet);
            System.err.println("SqlStoredProcedure.addBoard - " + res);
        } catch (SQLException ex) {
            res = Result.ERROR;
            System.err.println("SqlStoredProcedure.addBoard - " + ex);
        }
        return res;
    }
    public Result getAllBoards(Player player, LinkedList<Board> boards) {
        Result res = Result.ERROR;
        Board board;
        try {
            // open connection to database
            Connection connection = this.open();
            // send data to database
            String procedureName = Request.GET_ALL_BOARDS.SP();
            PreparedStatement preparedStatement = connection.prepareStatement(procedureName);
            preparedStatement.setInt(1, player.getID());
        
            ResultSet resultSet = preparedStatement.executeQuery();
            // get result from database
            while (resultSet.next()) {
                board = new Board();
                board.setBoardID(resultSet.getInt(1));
                board.setStrart(resultSet.getString(2));
                board.setSolution(resultSet.getString(3));
                board.setNumberOfSolvers(resultSet.getInt(4));
                board.setBestTime(resultSet.getString(5));
                if (player.getIsUser()) board.setStatus(resultSet.getString(6));
                boards.add(board);
                res = Result.SUCCESS;
            }
            // close connection
            this.close(connection, preparedStatement, resultSet);
            System.err.println("SqlStoredProcedure.getAllBoards - " + res);
        } catch (SQLException ex) {
            res = Result.ERROR;
            System.err.println("SqlStoredProcedure.getAllBoards - " + ex);
        }
        return res;
    }
    public Result getGame(Player player, Board board, Game game) {
        Result res = Result.ERROR;
        try {
            // open connection to database
            Connection connection = this.open();
            // send data to database
            String procedureName = Request.GET_GAME.SP();
            PreparedStatement preparedStatement = connection.prepareStatement(procedureName);
            preparedStatement.setInt(1, player.getID());
            preparedStatement.setInt(2, board.getBoardID());
            
            ResultSet resultSet = preparedStatement.executeQuery();
            
            // get result from database
            if (resultSet.next()) {
                game.setGameID(resultSet.getInt(1));
                game.setSaveBoard(resultSet.getString(2));
                game.setGameTime(resultSet.getLong(3));
                game.setNumberOfMistakes(resultSet.getInt(4));
                game.setIsSolved(resultSet.getBoolean(5));
                res = Result.SUCCESS;
            }
            // close connection
            this.close(connection, preparedStatement, resultSet);
            System.err.println("SqlStoredProcedure.getGame - " + res);
        } catch (SQLException ex) {
            res = Result.ERROR;
            System.err.println("SqlStoredProcedure.getGame - " + ex);
        }
        return res;
    }
    public Result updateGame(Game game) {
        Result res = Result.ERROR;
        try {
            // open connection to database
            Connection connection = this.open();
            // send data to database
            String procedureName = Request.UPDATE_GAME.SP();
            PreparedStatement preparedStatement = connection.prepareStatement(procedureName);

            preparedStatement.setInt(1, game.getGameID());    
            preparedStatement.setString(2, game.getStringSaveBoard());
            preparedStatement.setString(3, game.getStringGameTime());
            preparedStatement.setInt(4, game.getNumberOfMistakes());
            preparedStatement.setInt(5, (game.getIsSolved())?(1):(0));

            ResultSet resultSet = preparedStatement.executeQuery();
            
            // get result from database
            if (resultSet.next()) {
                res = Result.getResult(resultSet.getInt(1));
            }
            // close connection
            this.close(connection, preparedStatement, resultSet);
            System.err.println("SqlStoredProcedure.updateGame - " + res);
        } catch (SQLException ex) {
            res = Result.ERROR;
            System.err.println("SqlStoredProcedure.updateGame - " + ex);
        }
        return res;
    }
    public Result updateUsername(Player player) {
        Result res = Result.ERROR;
        try {
            // open connection to database
            Connection connection = this.open();
            // send data to database
            String procedureName = Request.UPDATE_USERNAME.SP();
            PreparedStatement preparedStatement = connection.prepareStatement(procedureName);
            preparedStatement.setInt(1, player.getID());
            preparedStatement.setString(2, player.getUsername());
        
            ResultSet resultSet = preparedStatement.executeQuery();
            // get result from database
            if (resultSet.next()) {
                res = Result.getResult(resultSet.getInt(1));
            }
            // close connection
            this.close(connection, preparedStatement, resultSet);
            System.err.println("SqlStoredProcedure.updateUsername - " + res);
        } catch (SQLException ex) {
            res = Result.ERROR;
            System.err.println("SqlStoredProcedure.updateUsername - " + ex);
        }
        return res;
    }
    public Result updatePassword(User player) {
        Result res= Result.ERROR
                ;
        try {
            // open connection to database
            Connection connection = this.open();
            // send data to database
            String procedureName = Request.UPDATE_PASSWORD.SP();
            PreparedStatement preparedStatement = connection.prepareStatement(procedureName);
            preparedStatement.setInt(1, player.getID());
            preparedStatement.setString(2, player.getPassword());
        
            ResultSet resultSet = preparedStatement.executeQuery();
            // get result from database
            if (resultSet.next()) {
                res = Result.getResult(resultSet.getInt(1));
            }
            // close connection
            this.close(connection, preparedStatement, resultSet);
            System.err.println("SqlStoredProcedure.updatePassword - " + res);
        } catch (SQLException ex) {
            res = Result.ERROR;
            System.err.println("SqlStoredProcedure.updatePassword - " + ex);
        }
        return res;
    }
    public Result loguot(Player logout) {
        Result res;
        try {
            // open connection to database
            Connection connection = this.open();
            // send data to database
            String procedureName = Request.LOGOUT.SP();
            PreparedStatement preparedStatement = connection.prepareStatement(procedureName);
            preparedStatement.setInt(1, logout.getID());
        
            preparedStatement.executeQuery();
            res = Result.SUCCESS;
            // close connection
            this.close(connection, preparedStatement, null);
            System.err.println("SqlStoredProcedure.loguot - " + res);
        } catch (SQLException ex) {
            res = Result.ERROR;
            System.err.println("SqlStoredProcedure.loguot - " + ex);
        }
        return res;
    }
}
