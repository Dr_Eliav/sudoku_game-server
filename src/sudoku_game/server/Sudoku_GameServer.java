
package sudoku_game.server;

import java.net.*;
import java.io.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Responsible for receiving client requests, and creating a new request thread,
 * Three steps:
 * 1- Listening to port
 * 2- Creating a socket
 * 3- Creating a new thread
 * 
 * @author EliavBuskila
 */
public class Sudoku_GameServer {

    private static final int MAX_CONNECTIONS = 10;
    private static final int PORT = 5056;
    private static final ExecutorService POOL = Executors.newFixedThreadPool(MAX_CONNECTIONS);
    
    public static void main(String[] args) {
        
        try {
            ServerSocket serverSocket = new ServerSocket(PORT);
            while (true) {
                // waiting to client connect 
                Socket socket = serverSocket.accept();
                
                // a new client is connected
                System.err.println("[IP: "+ socket.getInetAddress() + "]");
                
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());

                // create a new thread object
                ClientHandler thread = new ClientHandler(socket, ois, oos);
                POOL.execute(thread);
            }
        } catch (IOException ex) {
            System.err.println("SudokuGame_Server - Error");
        }
    }
}
